

class DriversBase:
    __slots__ = ('DB_NAME', 'DB_PASSWORD', 'DB_HOSTNAME', 'DB_PORT', 'DB_USERNAME')

    DB_NAME: str | None
    DB_PASSWORD: str | None
    DB_HOSTNAME: str | None
    DB_PORT: str | None
    DB_USERNAME: str | None
    ARGS: str | None

    def __init__(self,
                 DB_NAME=None,
                 DB_USERNAME=None,
                 DB_PASSWORD=None,
                 DB_HOSTNAME=None,
                 DB_PORT=None,
                 ARGS=''
                 **kwargs
                 ):

        self.DB_NAME = DB_NAME
        self.DB_USERNAME = DB_USERNAME
        self.DB_PASSWORD = DB_PASSWORD
        self.DB_HOSTNAME = DB_HOSTNAME
        self.DB_PORT = DB_PORT
        self.ARGS = ARGS

    def createDump(self, fileName: str) -> bool:
        pass

    def createPreProcess(self):
        pass