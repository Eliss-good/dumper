import os
from Drivers.DriversBase import DriversBase


class DriverPostgresSql(DriversBase):

    def createPreProcess(self):
        # with open('/.pgpass', 'w') as file:
        #     file.write(f"{self.DB_HOSTNAME}:{self.DB_PORT}:{self.DB_NAME}:{self.DB_USERNAME}:{self.DB_PASSWORD}")
        ...
    def createDump(self, fileName: str):
        resultStatusDump = os.WEXITSTATUS(
            os.system(f"PGPASSWORD={self.DB_PASSWORD} pg_dump -U {self.DB_USERNAME} -d {self.DB_NAME} -h {self.DB_HOSTNAME} --port {self.DB_PORT} > {fileName}"))

        return True if resultStatusDump == 0 else False



# -U {self.DB_USERNAME} -W  -d {self.DB_NAME} -h {self.DB_HOSTNAME} --port {self.DB_PORT}