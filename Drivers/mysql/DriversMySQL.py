import os
from Drivers.DriversBase import DriversBase
import logging


class DriversMySQL(DriversBase):

    def createPreProcess(self):
        with open('mysqldump.cnf', 'w') as file:
            file.write(f"[mysqldump] \nuser={self.DB_USERNAME} \npassword={self.DB_PASSWORD}")

    def createDump(self, fileName: str):
        logging.log(logging.INFO, f"mysqldump --defaults-file=mysqldump.cnf {self.ARGS} -h {self.DB_HOSTNAME} -P {self.DB_PORT} {self.DB_NAME} > {fileName}")

        
        resultStatusDump = os.WEXITSTATUS(
            os.system(f"mysqldump --defaults-file=mysqldump.cnf  {self.ARGS}  -h {self.DB_HOSTNAME} -P {self.DB_PORT} {self.DB_NAME} > {fileName}"))

        return True if resultStatusDump == 0 else False



