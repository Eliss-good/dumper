from HandlerDb import HandlerDb
from logging.config import fileConfig

fileConfig('./configlogger.ini')

if __name__ == '__main__':
    handlerDump = HandlerDb()
    handlerDump.chronoDump()