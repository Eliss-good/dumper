# ENV

1. DB_NAME
2. DB_PASSWORD
3. DB_HOSTNAME
4. DB_PORT
5. DB_USERNAME
6. TYPE_DRIVER ('MYSQL', 'POSTGRESQL')
7. INTERVAL_CREATE_DUMP_BY_SECOND
8. INTERVAL_DELETE_DUMPS_BY_SECOND - кол-во секунд через которые нужно удалить старые дампы

# VOLUME

/dumps - all dumps this project
logs.log - all logs dumper

