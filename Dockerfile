FROM ubuntu:latest

ENV TZ=Europe/Kiev
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get install -y wget && apt-get install -y lsb-release

RUN sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
RUN  wget -qO- https://www.postgresql.org/media/keys/ACCC4CF8.asc | tee /etc/apt/trusted.gpg.d/pgdg.asc &>/dev/nul && apt-get install -y postgresql postgresql-client 

RUN apt-get update && \
    apt-get install -y python3 && \
    apt-get install -y python3.12-venv   && \
    apt-get install -y mysql-client && \
    apt-get install -y postgresql postgresql-client

RUN mkdir /var/run/mysqld
RUN mkfifo /var/run/mysqld/mysqld.sock
RUN chown -R root /var/run/mysqld

RUN apt-get -y install python3-pip
RUN python3 -m venv venv

COPY ./ .
RUN . /venv/bin/activate && pip install -r /req.txt
CMD . /venv/bin/activate && python3 main.py



