from dotenv.main import load_dotenv, dotenv_values
from pydantic import BaseModel
from datetime import datetime, timezone, timedelta
from dateutil import parser
from uuid import uuid4
import logging
import time
import os

from Drivers.mysql.DriversMySQL import DriversMySQL
from Drivers.postgresql.DriverPostgreSql import DriverPostgresSql
from Drivers.DriversBase import DriversBase

load_dotenv()


drivers = {"MYSQL": DriversMySQL,
           "POSTGRESQL": DriverPostgresSql}


class ValidatorSettingDump(BaseModel):
    DB_NAME: str
    DB_PASSWORD: str
    DB_HOSTNAME: str
    DB_PORT: str
    DB_USERNAME: str
    TYPE_DRIVER: str
    INTERVAL_CREATE_DUMP_BY_SECOND: int
    INTERVAL_DELETE_DUMPS_BY_SECOND: int


class HandlerDb:
    settingDump: ValidatorSettingDump
    driver: DriversBase | None

    def __init__(self):
        envDict = {
            "DB_NAME": os.getenv("DB_NAME"),
            "DB_PASSWORD": os.getenv("DB_PASSWORD"),
            "DB_HOSTNAME": os.getenv("DB_HOSTNAME"),
            "DB_PORT": os.getenv("DB_PORT"),
            "ARGS": os.getenv("ARGS", ''),
            "DB_USERNAME": os.getenv("DB_USERNAME"),
            "TYPE_DRIVER": os.getenv("TYPE_DRIVER"),
            "INTERVAL_CREATE_DUMP_BY_SECOND": int(os.getenv('INTERVAL_CREATE_DUMP_BY_SECOND')),
            "INTERVAL_DELETE_DUMPS_BY_SECOND": int(os.getenv('INTERVAL_DELETE_DUMPS_BY_SECOND', 0))
        }
        
        #dotenv_values('./.env')
        self.settingDump = ValidatorSettingDump(**envDict)
        self.driver = drivers.get(self.settingDump.TYPE_DRIVER)(**self.settingDump.dict())

    def createNowTimeToStr(self) -> str:
        return datetime.now(timezone.utc).isoformat()

    def findFileDumps(self):
        try:
            root, dirs, files = os.walk('./dumps').__next__()
        except StopIteration as e:
            raise Exception('Такой директории не существует')
        return files

    def deleteFile(self, nameFile: str):
        print('удаляю ', nameFile)
        os.remove(f'./dumps/{nameFile}')

    def findDeleteFile(self):
        for filePath in self.findFileDumps():
            dateParse: datetime | None = len(filePath.split('_')) == 2 and parser.parse(filePath.split('_')[1].split('.')[0])
            dateParse and  dateParse + timedelta(seconds=self.settingDump.INTERVAL_DELETE_DUMPS_BY_SECOND) <= datetime.now() and self.deleteFile(
                filePath)


    def chronoDump(self):
        self.driver.createPreProcess()
        counterDeleteSecond: int = 0

        while True:
            newNameDump = f"./dumps/{uuid4()}_{self.createNowTimeToStr()}.sql"
            resultStatusDump = self.driver.createDump(newNameDump)
            logging.log(logging.INFO if resultStatusDump else logging.ERROR, f"Create new dump {newNameDump}")

            for oneSec in range(0, self.settingDump.INTERVAL_CREATE_DUMP_BY_SECOND):
                time.sleep(1)
                counterDeleteSecond += 1 if self.settingDump.INTERVAL_DELETE_DUMPS_BY_SECOND != 0 else 0
                if counterDeleteSecond and counterDeleteSecond == self.settingDump.INTERVAL_DELETE_DUMPS_BY_SECOND:
                    self.findDeleteFile()
                    counterDeleteSecond = 0

                logging.log(logging.INFO,
                            f"Wait new dump {oneSec}/{self.settingDump.INTERVAL_CREATE_DUMP_BY_SECOND}")








